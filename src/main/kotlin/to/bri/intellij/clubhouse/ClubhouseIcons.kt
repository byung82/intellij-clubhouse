package to.bri.intellij.clubhouse

import com.intellij.ui.IconManager
import javax.swing.Icon

object ClubhouseIcons {

    /** The main plugin icon. **/
    val Shortcut = load("/icons/clubhouse/shortcut.svg")

    /** Task item icon for bugs. */
    val Bug = load("/icons/clubhouse/bug.svg")

    /** Task item icon for chores. */
    val Chore = load("/icons/clubhouse/chore.svg")

    /** Task item icon for features.*/
    val Feature = load("/icons/clubhouse/feature.svg")

    private fun load(path: String): Icon {
        return IconManager.getInstance().getIcon(path, ClubhouseIcons::class.java)
    }

}
