package to.bri.intellij.clubhouse.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class Story(
    @Json(name = "id")
    val id: Int,

    @Json(name = "app_url")
    val url: String,

    @Json(name = "name")
    val name: String,

    @Json(name = "story_type")
    val type: String,

    @Json(name = "description")
    val description: String,

    @Json(name = "completed")
    val completed: Boolean,

    @Json(name = "comments")
    val comments: List<Comment>,

    @Json(name = "created_at")
    val createdAt: Date,

    @Json(name = "updated_at")
    val updatedAt: Date? = null

)
